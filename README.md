# Data request for TeWiki diff post

[T304312](https://phabricator.wikimedia.org/T304312)

## Notebooks

<dl>
  <dt><a href="telugu_editors.ipynb">telugu_editors</a></dt>
  <dd>Uses MediaWiki <a href="https://www.mediawiki.org/wiki/Manual:User_table"><code>user</code></a> table to find user IDs for the provided usernames</dd>
  <dd>Creates the table <code>bearloga.telugu_editors</code> in Hive</dd>
  <dt><a href="village_articles_query.ipynb">village_articles_query</a></dt>
  <dd>Uses MediaWiki tables such as <a href="https://www.mediawiki.org/wiki/Manual:Categorylinks_table"><code>categorylinks</code></a> and <a href="https://www.mediawiki.org/wiki/Manual:Page_table"><code>page</code></a> to find relevant articles</dd>
  <dt><a href="village_articles_bind.ipynb">village_articles_bind</a></dt>
  <dd>Binds the different levels of found articles into a single dataset to be loaded into Hive</dd>
  <dt><a href="village_articles_load.ipynb">village_articles_load</a></dt>
  <dd>Creates and populates the table <code>bearloga.village_articles</code></dd>
  <dt><a href="village_articles_edits.ipynb">village_articles_edits</a></dt>
  <dd>Uses <a href="https://wikitech.wikimedia.org/wiki/Analytics/Data_Lake/Edits/MediaWiki_history">MediaWiki History</a> together with <code>bearloga.telugu_editors</code> and <code>bearloga.village_articles</code> tables to generate a dataset of edits made by the specified users on all the found articles</dd>
  <dd>Creates the table <code>bearloga.village_articles_edits</code></dd>
  <dt><a href="deliverable_report.ipynb">deliverable_report</a></dt>
  <dd>Calculates the numbers requested</dd>
  <dt><a href="deliverable_dataset.ipynb">deliverable_dataset</a></dt>
  <dd>Creates a TSV file of the compiled "village articles edits" data</dd>
</dl>
